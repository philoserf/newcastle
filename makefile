.DEFAULT_GOAL := help
.PHONY: help
help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

MARKDOWN_FILES := $(shell git ls-files|grep -e '\.md')
YAML_FILES := $(shell git ls-files|grep -e '\.y[a]*ml')
SHELL_FILES := $(shell git ls-files|grep -e 'files/shell' | grep -ve '.keep')

.PHONY: vet
vet: ## Validate Markdown and YAML files
	mdl $(MARKDOWN_FILES)
	yamllint $(YAML_FILES)
	shellcheck -x $(SHELL_FILES)
	ansible-lint main.yml

.PHONY: check
check: ## Run the Ansible playbook in check mode
	ansible-playbook main.yml --check

.PHONY: run
run: ## Run the Ansible playbook
	ansible-playbook main.yml

.PHONY: setup
setup: ## Install Ansible (if required)
	./setup

.PHONY: setup-dev
setup-dev: ## Install Ansible and project development dependencies
	./setup dev
