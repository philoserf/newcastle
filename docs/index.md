# Ansible for a local macOS host

> An example of using Ansible to configure macOS.

## Usage

```plain
> make
check                    Run the Ansible playbook in check mode
run                      Run the Ansible playbook
setup-dev                Install Ansible and project development dependencies
setup                    Install Ansible (if required)
vet                      Validate Markdown and YAML files
```
