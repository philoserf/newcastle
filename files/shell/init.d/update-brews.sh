# shellcheck disable=SC2148,SC2164

update-brews() {
	echo 'Starting brew updates'
	pushd -q "$HOME"
	brew update
	brew upgrade
	brew cask upgrade
	brew cleanup -s
	brew cask cleanup
	brew prune
	popd -q
}
