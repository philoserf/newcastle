# shellcheck disable=SC2148

rubocop-ready() {
	touch .rubocop.yml
	rubocop --auto-correct
	rubocop --auto-gen-config
	echo inherit_from: .rubocop_todo.yml >>.rubocop.yml
}
