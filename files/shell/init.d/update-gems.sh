# shellcheck disable=SC2148,SC2164

update-gems() {
	pushd -q "$HOME"
	bundle update
	bundle clean --force
	popd -q
}
