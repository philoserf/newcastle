# shellcheck disable=SC2148,SC2164

update-all() {
  update-brews
  update-gems
  apm upgrade -c false
  rbenv update
  rbenv update-rubies
  antigen update
  mas upgrade
  softwareupdate --install --all --restart
}
