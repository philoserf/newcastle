# shellcheck disable=SC2148

alias atom='atom-beta'
alias apm='apm-beta'
alias be='bundle exec'
alias git='hub'
alias k='kubectl'
alias kctx='kubectx'
alias kns='kubens'
alias ll="ls -FrtohpqGl"
alias lla='ll -A'
