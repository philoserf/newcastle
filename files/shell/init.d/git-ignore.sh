# shellcheck disable=SC2148

function git-ignore() {
	curl -sL https://www.gitignore.io/api/\$@
}
